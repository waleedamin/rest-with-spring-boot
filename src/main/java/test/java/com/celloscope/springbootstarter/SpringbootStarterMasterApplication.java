package test.java.com.celloscope.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootStarterMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootStarterMasterApplication.class, args);
	}

}
