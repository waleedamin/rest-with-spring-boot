package test.java.com.celloscope.springbootstarter.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class controller {

	
	@GetMapping(path="/hello")
	public String myController() {
		
		return "Hello From Spring";
	}
	
	
	@GetMapping(path="/hello/{name}")
	public String myControllerWithParam(@PathVariable ("name") String name) {
		
		return "Hello Celloscope from "+name;
	}
	
}
